"use strict"

// 1) Метод обьекта это свойство обьекта в котором находиться функция
//    которая может работать с остальными свойствами обьекта.
// 2) Значения свойств могут являться любым типом данных, включая другие объекты и функции.
// 3) Объект это ссылочный тип данных и означает что при прямом присваивании обьекта
//    переменной, она будет ссылкой на этот обьект.

function createNewUser(){
    let usersFirstName = prompt("Enter your name:");
    let usersLastName = prompt("Enter your last name:");
    const newUser = {
        firstName: usersFirstName,
        lastName: usersLastName,
        getLogin(){

            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
        },
        setFirstName(val){
            Object.defineProperty(newUser, 'firstName', {
                writable: true
             });
             this.firstName = val
             Object.defineProperty(newUser, 'firstName', {
                writable: false
             });
        },
        setLastName(val){
            Object.defineProperty(newUser, 'lastName', {
                writable: true
             });
            this.lastName = val
            Object.defineProperty(newUser, 'lastName', {
                writable: false
             });
        }
    }
    Object.defineProperty(newUser, 'firstName', {
       writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        writable: false
     });
    return newUser;
}
const newUser = createNewUser();
console.log(newUser.getLogin());
//  я не впевнений, що використання Object.defineProperty так багато разів
//  є самим оптимальним рішенням, але усе працює, можливо є інші варіанти?